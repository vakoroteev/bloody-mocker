
package ru.deltasolutions.activator.rt.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for orderState complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="orderState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://ws.rt.activator.deltasolutions.ru/}order"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="serviceState" type="{http://ws.rt.activator.deltasolutions.ru/}serviceState" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="resultCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="resultText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "orderState", propOrder = {
    "serviceState",
    "resultCode",
    "resultText"
})
public class OrderState
    extends Order
{

    @XmlElement(nillable = true)
    protected List<ServiceState> serviceState;
    @XmlElement(required = true)
    protected String resultCode;
    @XmlElementRef(name = "resultText", type = JAXBElement.class, required = false)
    protected JAXBElement<String> resultText;

    /**
     * Gets the value of the serviceState property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceState property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceState().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceState }
     * 
     * 
     */
    public List<ServiceState> getServiceState() {
        if (serviceState == null) {
            serviceState = new ArrayList<ServiceState>();
        }
        return this.serviceState;
    }

    /**
     * Gets the value of the resultCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultCode() {
        return resultCode;
    }

    /**
     * Sets the value of the resultCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultCode(String value) {
        this.resultCode = value;
    }

    /**
     * Gets the value of the resultText property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getResultText() {
        return resultText;
    }

    /**
     * Sets the value of the resultText property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setResultText(JAXBElement<String> value) {
        this.resultText = value;
    }

    public OrderState() {}

    public OrderState(Order order) {
        this.setRequestId(order.getRequestId());
        this.setOrderId(order.getOrderId());
        this.setTimestamp(order.getTimestamp());
        this.setScheduledTimestamp(order.getScheduledTimestamp());
        this.setCallbackEndpoint(order.getCallbackEndpoint());
        this.setOriginator(order.getOriginator());
        this.setOperation(order.getOperation());

        for (Service service: order.getService()) {
            ServiceState serviceState = new ServiceState(service);
            this.getServiceState().add(serviceState);
        }

    }

}
