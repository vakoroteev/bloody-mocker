package context;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Katy on 15.03.2016.
 */
public class ApplicationContext {
    private static ApplicationContext instance;
    static Map<String, String> properties = new HashMap<>();

    private ApplicationContext() throws IOException {
        String configFileName = "config.properties";
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName)) {
            Properties prop = new Properties();
            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("Property file '" + configFileName + "' not found");
            }

            properties.put("resultCode", prop.getProperty("resultCode"));
            properties.put("statusCodeValidate", prop.getProperty("statusCodeValidate"));
            properties.put("statusCodeActivate", prop.getProperty("statusCodeActivate"));
            properties.put("statusCodeCommit", prop.getProperty("statusCodeCommit"));
            properties.put("statusCodeSynch", prop.getProperty("statusCodeSynch"));
            properties.put("resultText", prop.getProperty("resultText"));
            properties.put("statusText", prop.getProperty("statusText"));
            properties.put("serviceOperation", prop.getProperty("serviceOperation"));

            properties.put("scheduleTimeValidate", prop.getProperty("scheduleTimeValidate"));
            properties.put("scheduleTimeActivate", prop.getProperty("scheduleTimeActivate"));
            properties.put("scheduleTimeCommit", prop.getProperty("scheduleTimeCommit"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getPropertyByName(String name) {
        try {
            if (instance == null) {
                instance = new ApplicationContext();
            }
            return properties.get(name);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
