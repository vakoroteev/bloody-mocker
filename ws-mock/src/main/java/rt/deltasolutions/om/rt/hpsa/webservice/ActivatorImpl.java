package rt.deltasolutions.om.rt.hpsa.webservice;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import my.pack.mockinterface.MockInterface;
import my.pack.mockinterface.requests.SetResponseRequest;
import my.pack.mockinterface.responses.AssertInfo;
import ru.deltasolutions.activator.rt.ws.*;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.bind.JAXBElement;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Katy on 11.03.2016.
 */
@WebService(endpointInterface = "ru.deltasolutions.activator.rt.ws.Activator")
public class ActivatorImpl implements Activator, MockInterface {
    private static ObjectFactory objectFactory = new ObjectFactory();

    private static HashMap<String, String> hm;

    @Deprecated
    public OrderOperations retrieveOrderOperations(Order order) {
        return null;
    }

    @WebMethod
    public OrderState placeOrder(Order order) {
        OrderState orderState = new OrderState(order);
        System.out.println(hm);
        System.out.println("Start PlaceOrder \n");

        setParameters(order.getOperation(), order);

        orderState.setResultCode(hm.get("resultCode"));
        JAXBElement<String> resultText =
            objectFactory.createOrderStateResultText(hm.get("resultText"));
        orderState.setResultText(resultText);

        for (ServiceState serviceState : orderState.getServiceState()) {
            serviceState.setStatusCode(hm.get("statusCodeSynch"));
            JAXBElement<String> statusText =
                objectFactory.createServiceStateStatusText(hm.get("statusText"));
            serviceState.setStatusText(statusText);
        }

        return orderState;
    }

    private void setParameters(String operation, Order order) {
        RequestParameters requestParameters = RequestParameters.getInstance();
        if (operation.equals("Submit")) {
            System.out.println("Set parameters Activate \n");
            requestParameters.setParameters(order, hm.get("statusCodeValidate"));
            requestParameters.setParameters(order, hm.get("statusCodeActivate"));
            Executor.scheduleActivate(order);
        } else if (operation.equals("Update")) {
            System.out.println("Set parameters Commit \n");
            requestParameters.setParameters(order, hm.get("statusCodeCommit"));
            Executor.scheduleCommit(order);
        }
    }

    @WebMethod
    public OrderState retrieveOrderState(Order order) {
        OrderState orderState = new OrderState(order);
        orderState.setResultCode("0");
        for (ServiceState serviceState : orderState.getServiceState()) {
            serviceState.setStatusCode("COMPLETED");
        }
        return orderState;
    }

    @Override
    public void setResponse(SetResponseRequest request) {
        ObjectMapper om = new ObjectMapper();
        TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {
        };
        try {
            hm = om.readValue(request.getJsonResponse(), typeRef);
            System.out.println(hm);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setAsync(Map<String, ObjectNode> asyncRequests) {

    }

    @Override
    public AssertInfo getAssertInfo() {
        return null;
    }
}
