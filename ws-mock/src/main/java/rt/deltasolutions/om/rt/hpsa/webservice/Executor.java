package rt.deltasolutions.om.rt.hpsa.webservice;

import context.ApplicationContext;
import ru.deltasolutions.activator.rt.ws.Order;
import ru.deltasolutions.activator.rt.ws.Service;
import ru.deltasolutions.activator.rt.ws.ServiceState;

import javax.xml.bind.JAXBElement;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Katy on 18.03.2016.
 */
public class Executor {
    private static ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

    public static void scheduleActivate(Order order) {
        try {
            System.out.println("Start schedule activate \n");
            String instanceId = ((Service) (order.getService().toArray()[0])).getInstanceId();
            System.out.println("instance get " + instanceId);

            ActivatorCallbackImpl activatorCallback =
                    new ActivatorCallbackImpl(instanceId, RequestParameters.statusCode.VALIDATED.toString());
            scheduledExecutorService.schedule(activatorCallback,
                    Long.valueOf(ApplicationContext.getPropertyByName("scheduleTimeValidate")), TimeUnit.SECONDS);
            ActivatorCallbackImpl activatorCallback2
                    = new ActivatorCallbackImpl(instanceId, RequestParameters.statusCode.COMPLETED.toString());
            scheduledExecutorService.schedule(activatorCallback2,
                    Long.valueOf(ApplicationContext.getPropertyByName("scheduleTimeActivate")), TimeUnit.SECONDS);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void scheduleCommit(Order order) {
        try {
            System.out.println("Start schedule commit \n");
            String instanceId = ((Service) (order.getService().toArray()[0])).getInstanceId();
            System.out.println("instance get " + instanceId);

            ActivatorCallbackImpl activatorCallback =
                    new ActivatorCallbackImpl(instanceId, RequestParameters.statusCode.COMMITED.toString());
            scheduledExecutorService.schedule(activatorCallback,
                    Long.valueOf(ApplicationContext.getPropertyByName("scheduleTimeCommit")), TimeUnit.SECONDS);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
