package rt.deltasolutions.om.rt.hpsa.webservice;

import javax.xml.ws.Endpoint;

/**
 * Created by Katy on 11.03.2016.
 */
public class ActivatorWebServicePublisher {
    public static void main(String... args) {
        try {
            Endpoint.publish("http://127.0.0.1:20081/hpsa-if-ws/activator", new ActivatorImpl());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
