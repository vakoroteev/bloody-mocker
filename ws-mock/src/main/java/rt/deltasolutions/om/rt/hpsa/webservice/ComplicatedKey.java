package rt.deltasolutions.om.rt.hpsa.webservice;

/**
 * Created by Katy on 21.03.2016.
 */
public class ComplicatedKey {
    //private String orderId;
    private String instanceId;
    private String statusCode;

    public ComplicatedKey(String instanceId, String statusCode) {
        //this.orderId = orderId;
        this.instanceId = instanceId;
        this.statusCode = statusCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ComplicatedKey that = (ComplicatedKey) o;

        return instanceId.equals(that.instanceId) && statusCode.equals(that.statusCode);

    }

    @Override
    public int hashCode() {
        int result = instanceId.hashCode();
        result = 31 * result + statusCode.hashCode();
        return result;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }
}
