package rt.deltasolutions.om.rt.hpsa.webservice;

import context.ApplicationContext;
import ru.deltasolutions.activator.rt.ws.Order;
import ru.deltasolutions.activator.rt.ws.Service;

import java.util.*;

/**
 * Created by Katy on 16.03.2016.
 */

public class RequestParameters {
    public enum statusCode {
        VALIDATED {
            public String toString() {
                return "VALIDATED";
            }
        },
        COMPLETED {
                public String toString() {
                    return "COMPLETED";
                }
        },
        COMMITED {
            public String toString() {
                return "COMMITED";
            }
        }

    };
    private static Map<ComplicatedKey, ParametersOrder> parameters = new HashMap<ComplicatedKey, ParametersOrder>();
    private static volatile RequestParameters instance;

    private RequestParameters() {  }

    public class ParametersOrder {
        private String requestId;
        private String orderId;
        private String operation;
        private List<ParametersService> parametersService;

        public ParametersOrder(Order order) {
            requestId = order.getRequestId();
            operation = order.getOperation();
            orderId = order.getOrderId();

            for (Service service: order.getService()) {
                ParametersService parametersService = new ParametersService(service);
                this.getParametersService().add(parametersService);
            }
        }

        public List<ParametersService> getParametersService() {
            if (parametersService == null) {
                parametersService = new ArrayList<ParametersService>();
            }
            return this.parametersService;
        }

        public String getRequestId() {
            return requestId;
        }

        public void setRequestId(String requestId) {
            this.requestId = requestId;
        }

        public String getOperation() {
            return operation;
        }

        public void setOperation(String operation) {
            this.operation = operation;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }
    }

    public class ParametersService {
        private String instanceId;
        private String specId;
        private String action;

        public String getOperation() {
            return operation;
        }

        public void setOperation(String operation) {
            this.operation = operation;
        }

        private String operation;

        public ParametersService(Service service) {
            instanceId = service.getInstanceId();
            specId = service.getSpecId();
            action = service.getAction();
            operation = ApplicationContext.getPropertyByName("serviceOperation");
            System.out.println("Set service operation = " + operation + "\n");
        }

        public String getInstanceId() {
            return instanceId;
        }

        public void setInstanceId(String instanceId) {
            this.instanceId = instanceId;
        }

        public String getSpecId() {
            return specId;
        }

        public void setSpecId(String specId) {
            this.specId = specId;
        }

        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }
    }

    public void setParameters(Order order, String statusCode) {
        ParametersOrder parametersOrder = new ParametersOrder(order);
        String instanceId = ((Service) (order.getService().toArray()[0])).getInstanceId();
        ComplicatedKey key = new ComplicatedKey(instanceId,
            statusCode);
        parameters.put(key, parametersOrder);
    }


    public static RequestParameters getInstance() {
        RequestParameters localInstance = instance;
        if (localInstance == null) {
            synchronized (RequestParameters.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new RequestParameters();
                }
            }
        }
        return localInstance;
    }

    public ParametersOrder getRequestParametersByKeyMap(ComplicatedKey keyMap) {
        for (ComplicatedKey c: parameters.keySet()) {
            System.out.println(c.getInstanceId() +"  " +c.getStatusCode());
        }

        return parameters.get(keyMap);
    }

    public void deleteParametersByKeyMap(ComplicatedKey keyMap) {
        parameters.remove(keyMap);
    }

}
