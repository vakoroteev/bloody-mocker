package rt.deltasolutions.om.rt.hpsa.webservice;

import ru.deltasolutions.activator.rt.ws.*;

/**
 * Created by Katy on 16.03.2016.
 */
public class ActivatorCallbackImpl implements Runnable {
    private ComplicatedKey complicatedKey;

    public ActivatorCallbackImpl(String instanceId, String statusCode) {
        System.out.println("Set notification with " + instanceId + "\n");
        this.complicatedKey = new ComplicatedKey(instanceId, statusCode);
    }

    public void run() {
        System.out.println("Run notification with "
            + complicatedKey.getInstanceId() + " " + complicatedKey.getStatusCode());
        sendAsyncResponse();
    }

    private void sendAsyncResponse() {
        System.out.println("start asynch");
        ActivatorCallbackService activatorCallbackService = new ActivatorCallbackService();
        System.out.println("get port");
        ActivatorCallback activatorCallback = activatorCallbackService.getActivatorCallbackPort();
        System.out.println("send request");
        Result res = activatorCallback.notifyOrderState(getRequest());
        System.out.println(res);
    }

    private OrderState getRequest() {

        OrderState orderState = new OrderState();
        try {
            RequestParameters.ParametersOrder requestParameters =
                RequestParameters.getInstance().getRequestParametersByKeyMap(complicatedKey);
            System.out.println("get parameters" + requestParameters);

            String req = requestParameters.getRequestId();
            orderState.setRequestId(req);
            orderState.setOperation(requestParameters.getOperation());
            orderState.setOrderId(requestParameters.getOrderId());

            orderState.setResultCode("0");

            for (RequestParameters.ParametersService parametersService
                : requestParameters.getParametersService()) {
                ServiceState serviceState = new ServiceState();
                serviceState.setInstanceId(parametersService.getInstanceId());
                serviceState.setSpecId(parametersService.getSpecId());
                serviceState.setAction(parametersService.getAction());
                serviceState.setOperation(new ObjectFactory().createServiceOperation(parametersService.getOperation()));

                System.out.println("get request with operation" + parametersService.getOperation());
                serviceState.setStatusCode(complicatedKey.getStatusCode());

                orderState.getServiceState().add(serviceState);
            }

            RequestParameters.getInstance().deleteParametersByKeyMap(complicatedKey);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return orderState;
    }

}
