package my.pack.mockinterface;

import com.fasterxml.jackson.databind.node.ObjectNode;
import my.pack.mockinterface.requests.SetResponseRequest;
import my.pack.mockinterface.responses.AssertInfo;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Map;

@Path("/")
public interface MockInterface {

    @POST
    @Path("/configureResponse")
    @Consumes(MediaType.APPLICATION_JSON)
    void setResponse(SetResponseRequest request);

    @POST
    @Path("/configureAsync")
    @Consumes(MediaType.APPLICATION_JSON)
    void setAsync(Map<String, ObjectNode> asyncRequests);

    @GET
    @Path("/getAssertInfo")
    @Produces(MediaType.APPLICATION_JSON)
    AssertInfo getAssertInfo();

}
