package my.pack.mockinterface.responses;

import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.Data;

import java.util.HashMap;
import java.util.List;

@Data
public class AssertInfo {
    private HashMap<String, List<ObjectNode>> requests;
    private HashMap<String, List<ObjectNode>> responses;
}
