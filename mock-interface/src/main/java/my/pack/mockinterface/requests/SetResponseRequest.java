package my.pack.mockinterface.requests;

import lombok.Data;

@Data
public class SetResponseRequest {
    private String methodName;
    private String jsonResponse;
}
