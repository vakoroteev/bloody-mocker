package my.pack.mocker.config;

import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.Data;
import my.pack.mockinterface.responses.AssertInfo;

import java.util.Map;

@Data
public class MockConfig {
    private String name;
    private String address;
    private Map<String, ObjectNode> methodsResponses;
    private Map<String, ObjectNode> asyncRequests;
    private AssertInfo assertInfo;
}
