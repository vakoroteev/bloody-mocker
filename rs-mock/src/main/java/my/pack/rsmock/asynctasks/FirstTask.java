package my.pack.rsmock.asynctasks;

import lombok.Data;

@Data
public class FirstTask implements Runnable {

    private String name;
    private int time;

    @Override
    public void run() {
        System.out.println("First task: " + name);
    }
}
