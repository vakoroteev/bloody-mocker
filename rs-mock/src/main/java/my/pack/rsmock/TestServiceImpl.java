package my.pack.rsmock;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import my.pack.mockinterface.requests.SetResponseRequest;
import my.pack.mockinterface.responses.AssertInfo;
import my.pack.rsmock.asynctasks.FirstTask;
import my.pack.rsmock.asynctasks.SecondTask;
import my.pack.rsmock.request.TestServiceRequest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TestServiceImpl implements TestService {
    private static ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    private static HashMap<String, Object> responses = new HashMap<>();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private static final HashMap<String, List<ObjectNode>> statRequests = new HashMap<>();
    private static final HashMap<String, List<ObjectNode>> statResponses = new HashMap<>();

    @Override
    public Response testGetMethod(TestServiceRequest request) {
        String methodName = "testGetMethod";
        addToStatisticMap((ObjectNode) objectMapper.valueToTree(request), statRequests, methodName);
        Response response = (Response) responses.get(methodName);
        addToStatisticMap((ObjectNode) objectMapper.valueToTree(response), statResponses, methodName);
        return response;
    }

    // WARN! Side effect
    private void addToStatisticMap(ObjectNode statEntry, HashMap<String, List<ObjectNode>> statMap, String methodName) {
        List<ObjectNode> objectNodes = statMap.get(methodName);
        if (objectNodes == null) {
            objectNodes = new ArrayList<>();
        }
        objectNodes.add((ObjectNode) objectMapper.valueToTree(statEntry));
        statMap.put(methodName, objectNodes);
    }

    @Override
    public void setResponse(SetResponseRequest request) {
        Response response = null;
        try {
            response = objectMapper.readValue(request.getJsonResponse(), Response.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        responses.put(request.getMethodName(), response);
    }

    @Override
    public void setAsync(Map<String, ObjectNode> asyncRequests) {
        for (Map.Entry<String, ObjectNode> en : asyncRequests.entrySet()) {
            String key = en.getKey();
            try {
                switch (key) {
                    case "firstReq":
                        FirstTask firstTask = objectMapper.treeToValue(en.getValue(), FirstTask.class);
                        executorService.schedule(firstTask, firstTask.getTime(), TimeUnit.SECONDS);
                        break;
                    case "secondReq":
                        SecondTask secondTask = objectMapper.treeToValue(en.getValue(), SecondTask.class);
                        executorService.schedule(secondTask, secondTask.getTime(), TimeUnit.SECONDS);
                        break;
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public AssertInfo getAssertInfo() {
        AssertInfo assertInfo = new AssertInfo();
        assertInfo.setResponses(new HashMap<>(statResponses));
        assertInfo.setRequests(new HashMap<>(statRequests));
        statRequests.clear();
        statResponses.clear();
        return assertInfo;
    }
}
