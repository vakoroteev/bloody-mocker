package my.pack.rsmock.request;

import lombok.Data;

@Data
public class TestServiceRequest {
    private boolean b;
    private String s;
}
