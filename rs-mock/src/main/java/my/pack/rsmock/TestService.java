package my.pack.rsmock;

import my.pack.mockinterface.MockInterface;
import my.pack.rsmock.request.TestServiceRequest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/restTest")
public interface TestService extends MockInterface {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    Response testGetMethod(TestServiceRequest request);

}
