package my.pack.rsmock;

import lombok.Data;

@Data
public class Response {
    private int errorCode;
    private String errorMessage;
}
