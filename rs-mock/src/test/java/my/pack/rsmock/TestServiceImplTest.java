package my.pack.rsmock;

import my.pack.mocker.annotation.BloodyTest;
import my.pack.mocker.runner.BloodyMockerTestRunner;
import my.pack.rsmock.request.TestServiceRequest;
import org.junit.runner.RunWith;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


@RunWith(BloodyMockerTestRunner.class)
public class TestServiceImplTest {

    @BloodyTest(pathToConfig = "testFile.json")
    public void badTest() {
        String url = "http://localhost:8080/rs-mock/restTest";
        RestTemplate rt = new RestTemplate();
        TestServiceRequest request = new TestServiceRequest();
        request.setB(true);
        request.setS("IIIIHHH");
        ResponseEntity<Response> responseResponseEntity = rt.postForEntity(url, request, Response.class);
        System.out.println(responseResponseEntity.getBody());
    }

    @BloodyTest(pathToConfig = "testFile.json")
    public void goodTest() {
        String url = "http://localhost:8080/rs-mock/restTest";
        RestTemplate rt = new RestTemplate();
        TestServiceRequest request = new TestServiceRequest();
        request.setB(false);
        request.setS("IIIIHHH");
        ResponseEntity<Response> responseResponseEntity = rt.postForEntity(url, request, Response.class);
        System.out.println(responseResponseEntity.getBody());
    }

}
